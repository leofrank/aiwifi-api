<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use DB;

class ContactController extends Controller
{
    public function uploadCsv(Request $request)
    {
        $client = auth('client')->user();

        $file = $request->file('file');
        
        $csvData = file_get_contents($file->getPathname());
        
        $rows = str_getcsv($csvData, "\n");

        $total = count($rows);

        $loaded = 0;
        $unloaded = 0;

        foreach ($rows as $row) {
            $values = str_getcsv($row, ",");

            $contact = new Contact();
            $contact->name = $values[0];
            $contact->email = $values[1];
            $contact->client_id = $client->id;

            $response = Http::get('http://apilayer.net/api/check', [
                'access_key' => '7fa5ae664bf07e5f5f887bb234da1827',
                'email' => $contact['email'],
            ]);

            if ($response->ok() && $response['format_valid']) {
                 // Obtener la información de la API AgeAPI
            $agifyResponse = Http::withoutVerifying()->get('https://api.agify.io/', [
                'name' => $contact['name'],
            ]);

            $agifyData = $agifyResponse->json();
            
            $contact['age'] = $agifyData['age'] ?? null;

            $nationalizeResponse = Http::withoutVerifying()->get('https://api.nationalize.io/', [
                'name' => $contact['name'],
            ]);
            
            $nationalizeData = $nationalizeResponse->json();
            $countryData = $nationalizeData['country'][0] ?? null;
            $contact['nationality'] = $countryData['country_id'] ?? null;

            $genderizeResponse = Http::withoutVerifying()->get('https://api.genderize.io/', [
                'name' => $contact['name'],
            ]);

            $genderizeData = $genderizeResponse->json();
            $contact['gender'] = $genderizeData['gender'] ?? null;
               
            $contact->save();
            $loaded++;
            
            } else {
                $unloaded++;
            }

        }

        if($loaded > 0) {
            return response()->json([
                'message' => 'Se cargaron '. $loaded .' contactos de '. $total,
            ]);
        }
    }

    public function listContacts()
    {
        $contacts = Contact::all();

        return response()->json($contacts);
    }

    public function deleteContact($id)
    {
        $contact = Contact::findOrFail($id);
        $contact->delete();

        return response()->json(['message' => 'Contacto eliminado correctamente']);
    }

        public function getStatistics()
    {
        $totalContacts = Contact::count();
        $maleCount = Contact::where('gender', 'male')->count();
        $femaleCount = Contact::where('gender', 'female')->count();
        $Count = Contact::where('gender', 'female')->count();

        $ageStatistics = Contact::select('age')
            ->whereNotNull('age')
            ->groupBy('age')
            ->orderBy('age')
            ->get([
                'age',
                DB::raw('count(*) as count')
            ]);
            $ageStatisticsCount = $ageStatistics->map(function ($item) {
                $count = Contact::where('age', $item->age)->count();
                $item->count = $count;
                return $item;
            });

        $nationalityStatistics = Contact::select('nationality')
            ->whereNotNull('nationality')
            ->groupBy('nationality')
            ->orderBy('nationality')
            ->get([
                'nationality',
                DB::raw('count(*) as count')
            ]);
            $nationalityStatisticsCount = $nationalityStatistics->map(function ($item) {
                $count = Contact::where('nationality', $item->nationality)->count();
                $item->count = $count;
                return $item;
            });

        $statistics = [
            'total_contacts' => $totalContacts,
            'male_count' => $maleCount,
            'female_count' => $femaleCount,
            'age_stats' => $ageStatistics,
            'age_stats' => $ageStatisticsCount,
            'nationality_stats' => $nationalityStatistics,
            'nationality_stats' => $nationalityStatisticsCount
        ];

        return response()->json($statistics);
    }

}
