<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ContactController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Ruta para el registro de clientes
Route::post('clients/register', [ClientController::class, 'register']);
// Ruta para el login de clientes
Route::post('clients/login', [ClientController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    // Ruta para cargar el archivo Csv
    Route::post('contacts/upload-csv', [ContactController::class, 'uploadCsv']);
    // Ruta para listar los contactos
    Route::get('contacts', [ContactController::class, 'listContacts']);
    // Ruta para cargar obtener estadiaticas
    Route::get('contacts/statistics', [ContactController::class, 'getStatistics']);
    // Ruta para eliminar los contactos
    Route::delete('contacts/{id}', [ContactController::class, 'deleteContact']);
});