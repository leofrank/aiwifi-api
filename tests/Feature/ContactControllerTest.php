<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;
use App\Models\Client;
use App\Models\Contact;

class ContactControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testListContacts()
    {
        $client = Client::factory()->create();
        $contacts = Contact::factory()->count(3)->create([
            'client_id' => $client->id,
        ]);

        $token = $client->createToken('test_token')->plainTextToken;

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ];
        
        $response = $this->withHeaders($headers)->getJson('/api/contacts');

        $response->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'email',
                    'client_id',
                    'age',
                    'gender',
                    'nationality',
                    'created_at',
                    'updated_at',
                ],
            ])
            ->assertJsonCount(3);
    }


    public function testDeleteContact()
    {
        $client = Client::factory()->create();
        $contact = Contact::factory()->create([
            'client_id' => $client->id,
        ]);

        $token = $client->createToken('test_token')->plainTextToken;

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ];

        $response = $this->withHeaders($headers)->deleteJson('/api/contacts/' . $contact->id);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Contacto eliminado correctamente',
            ]);
        $this->assertDatabaseMissing('contacts', ['id' => $contact->id]);
    }

    public function testGetStatistics()
    {
        $client = Client::factory()->create();
        Contact::factory()->count(5)->create(
            [
                'client_id' => $client->id,
            ]
        );

        $token = $client->createToken('test_token')->plainTextToken;

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ];

        $response = $this->withHeaders($headers)->getJson('/api/contacts/statistics');

        $response->assertStatus(200)
            ->assertJsonStructure([
                'total_contacts',
                'male_count',
                'female_count',
                'age_stats' => [
                    '*' => [
                        'age',
                        'count',
                    ],
                ],
                'nationality_stats' => [
                    '*' => [
                        'nationality',
                        'count',
                    ],
                ],
            ]);
    }
}