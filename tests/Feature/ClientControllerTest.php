<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use App\Models\Client;
use Tests\TestCase;

class ClientControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testRegister()
    {
        $response = $this->post('/api/clients/register', [
            'name' => 'leo',
            'email' => 'leo@leo.com',
            'password' => 'password123',
        ]);

        $response->assertStatus(201)
            ->assertJson([
                'message' => 'Registrado con exito',
            ]);

        $data = $response->json();

        $this->assertArrayHasKey('token', $data);
        $this->assertNotNull($data['token']);
    }

    public function testLogin()
    {
        $client = Client::factory()->create([
            'email' => 'leo@leo.com',
            'password' => Hash::make('password123'),
        ]);

        $response = $this->post('/api/clients/login', [
            'email' => 'leo@leo.com',
            'password' => 'password123',
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Login exitoso',
            ]);

        $data = $response->json();

        $this->assertArrayHasKey('token', $data);
        $this->assertNotNull($data['token']);
    }
}
